import {Dropdown, Button, Nav} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useContext, useState } from 'react';
import AppContext from '../../contexts/AppContext';
import { Person, BagFill } from 'react-bootstrap-icons';
import Swal from 'sweetalert2';

export default function AppNavbarUsers() {

  const {user, setUser} = useContext(AppContext);

  const [userShow, setUserShow] = useState(false);
  const [cartShow, setCartShow] = useState(false);

  const navigate = useNavigate();

  const showUserDropdown = (e)=>{
      setUserShow(!userShow);
  }
  const hideUserDropdown = e => {
      setUserShow(false);
  }

  const showCartDropdown = (e)=>{
      setCartShow(!cartShow);
  }
  const hideCartDropdown = e => {
      setCartShow(false);
  }

  const logout = () => {
    localStorage.clear();
    setUser(null);
    Swal.fire('Logged out', '', 'info');
    navigate('/login');
}

  const navToLogin = () => {
    navigate('/login')
  }

  const navToRegister = () => {
    navigate('/register')
  }

  const handleUserLoggedIn = () => {
    return (!user || user.id === null) ? 
    <>
      <Button variant='outline-dark' onClick={navToRegister}>Register</Button>
      <Button variant='dark' className='ms-2' onClick={navToLogin}>Log In</Button>
    </>
    :
    <div className='d-flex'>
    <Nav>

      {
        (!user.isAdmin) ? 
        <Nav.Link className='m-0 p-0 d-flex align-items-center' onClick={() => navigate('/checkout')}>
        {`CART(${user.cart.length})`}
        </Nav.Link> :
        <Nav.Link className='m-0 p-0 d-flex align-items-center' onClick={() => navigate('/admin')}>
        ADMIN DASHBOARD
        </Nav.Link>
      }
    </Nav>
    <Dropdown className='appnav__user-toggle-cont' show={userShow} onMouseEnter={showUserDropdown} onMouseLeave={hideUserDropdown}>
       <Dropdown.Toggle className="ps-4 appnav__user-toggle">
         <Person/>
       </Dropdown.Toggle>

       <Dropdown.Menu className='appnav__user-menu'>
        <Dropdown.ItemText className='bg-dark text-light'>{`HELLO, ${user.name.split(' ')[0].toUpperCase()}`}</Dropdown.ItemText>
       {!user.isAdmin && <Dropdown.Item onClick={() => {navigate('/orders')}}>Orders</Dropdown.Item>}
       <Dropdown.Item onClick={logout}>Log Out</Dropdown.Item>
       </Dropdown.Menu>
    </Dropdown>
    </div>
  }

  return (
    // <Dropdown className='appnav__user-toggle-cont' show={show} onMouseEnter={showDropdown} onMouseLeave={hideDropdown}>
    //   <Dropdown.Toggle className="appnav__user-toggle">
    //     <PersonFill/>
    //   </Dropdown.Toggle>

    //   <Dropdown.Menu className='appnav__user-menu'>
    //     {handleUserLoggedIn()}
    //   </Dropdown.Menu>
    // </Dropdown>
    <div className='col-4 d-flex justify-content-end'>
      {handleUserLoggedIn()}
    </div>
  );
}