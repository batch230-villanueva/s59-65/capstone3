import { Container, Navbar, Nav } from 'react-bootstrap'
import AppNavbarUsers from './AppNavbarUsers'
import AppNavMenu from './AppNavMenu'
import AppNavMenuMobile from './AppNavMenuMobile'
import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'

const getWidth = () => {
  const {innerWidth} = window
  return innerWidth
}

export default function AppNavbar() {

    const navigate = useNavigate()
    
    const [width, setWidth] = useState(getWidth());
    
    useEffect(() => {
      const handleResize = () => {
        setWidth(getWidth())
      }
  
      window.addEventListener('resize', handleResize);
  
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, [])

    const handleBrandResponsive = () => {
      if (width < 576) {
        return 'D/a'
      } else {
        return 'DISCO/Atelier'
      }
    }

    return(
        <Navbar className='container-fluid d-flex justify-content-between px-2 px-lg-5 appnav'>
          <Nav className='col-4'>
            <Nav.Link onClick={() => {navigate('/')}}>HOME</Nav.Link>
          </Nav>
          <div className='col-4 appnav__brand'>
            <h5 className='m-0 ' onClick={() => navigate("/")}>{handleBrandResponsive()}</h5>
          </div>
          <AppNavbarUsers/>
        </Navbar>
    )
}