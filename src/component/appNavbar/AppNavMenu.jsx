import { useContext, useState } from "react";
import AppContext from '../../contexts/AppContext';
import { Nav } from'react-bootstrap';
import { useNavigate } from "react-router-dom";

export default function AppNavMenu () {

    const {user, setUser} = useContext(AppContext)

    const navigate = useNavigate();

    return(
        <Nav className="ms-2">
            <Nav.Link onClick={() => {navigate('/')}}>HOME</Nav.Link>
            <Nav.Link onClick={() => {navigate('#products')}}>PRODUCTS</Nav.Link>
        </Nav>
    )
}