import AppNavbar from './component/appNavbar/AppNavbar'
import Login from './routes/login/Login';
import Register from './routes/register/Register';
import Home from './routes/home/Home';
import AdminDash from './routes/adminDash/AdminDash';
import Product from './routes/product/Product';
import Checkout from './routes/checkout/Checkout';
import Orders from './routes/orders/Orders';
import Lost from './routes/lost/Lost';
import './App.css'
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { AppProvider } from './contexts/AppContext';

function App() {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')));
  const [products, setProducts] = useState([]);

  const [selectedProduct, setSelectedProduct] = useState();

  const unsetUser = () => {
    setUser(null)
    localStorage.removeItem('id');
  }

  const refreshUserDetails = (token) => {
    fetch('https://capstone-2-villanueva.onrender.com/users/getUser', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // (data.isAdmin) ? navigate('/admin') : navigate('/')

      const logUser = {
        name: `${data.firstName} ${data.lastName}`,
        id: data._id,
        isAdmin: data.isAdmin,
        cart: data.cart,
      }

      localStorage.setItem('user',JSON.stringify(logUser))

      setUser(logUser);

      // (data.isAdmin) ? navigate('/admin') : navigate('/')
    })
  }

  const refreshProducts = () => {
    fetch('https://capstone-2-villanueva.onrender.com/products/getActiveProducts')
        .then(res => res.json())
        .then(data => {
            setProducts(data)})
  }

const getProductName = (prodId) => {
   return fetch(`https://capstone-2-villanueva.onrender.com/products/getProductName/${prodId}`)
  .then(res => res.json())
  .then(data => {
    return data
  })
}

const getProductPrice = (prodId) => {
   return fetch(`https://capstone-2-villanueva.onrender.com/products/getProductPrice/${prodId}`)
  .then(res => res.json())
  .then(data => {
    return data
  })
}

const getUserName = (userId) => {
   return fetch(`https://capstone-2-villanueva.onrender.com/users/getUserName/${userId}`)
  .then(res => res.json())
  .then(data => {
    return data
  })
}

  useEffect(() => {
    setUser(JSON.parse(localStorage.getItem('user')));
  },[])

  return (
    <AppProvider value={{user, setUser, unsetUser, selectedProduct, setSelectedProduct, refreshUserDetails, products, setProducts, refreshProducts, getProductName, getProductPrice, getUserName}}>
      <Router>
        <Container fluid className='main'>
          <AppNavbar/>
          {/* <h1>Nicolette Casey</h1> */}
          <Routes>
            <Route path="/"  element={<Home/>}/>
            <Route path="/admin" element={<AdminDash/>}/>
            <Route path="/login"  element={<Login/>}/>
            <Route path="/register"  element={<Register/>}/>
            <Route path="/product"  element={<Product/>}/>
            <Route path="/checkout"  element={<Checkout/>}/>
            <Route path="/orders"  element={<Orders/>}/>
            <Route path="*"  element={<Lost/>}/>
          </Routes>
        </Container>
      </Router>
    </AppProvider>
  )
}

export default App
