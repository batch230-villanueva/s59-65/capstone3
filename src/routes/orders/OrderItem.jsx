import { Container } from "react-bootstrap"
import { useState, useEffect, useContext } from "react"
import AppContext from "../../contexts/AppContext"
import { CircleFill } from "react-bootstrap-icons";
import OrderItemProduct from "./OrderItemProduct";

export default function OrderItem ({order}) {

    const {getUserName, getProductName} = useContext(AppContext);

    const [userName, setUserName] = useState("");

    const convertDate = (isoString) => {
        const dateObj = new Date(isoString);
        const year = dateObj.getFullYear();
        const month = ('0' + (dateObj.getMonth() + 1)).slice(-2);
        const date = ('0' + dateObj.getDate()).slice(-2);
        const hours = ('0' + dateObj.getHours()).slice(-2);
        const minutes = ('0' + dateObj.getMinutes()).slice(-2);
        const seconds = ('0' + dateObj.getSeconds()).slice(-2);
        const fDate = `${date}-${month}-${year}`;
        const fTime = `${hours}:${minutes}:${seconds}`;
        return `${fDate} ${fTime}`;
    }

    useEffect(() => {
      getUserName(order.userId).then(result => {
        setUserName(result.name)
      })
    }, [])

    return (
        <Container className="d-flex flex-column flex-md-row w-100 h-100 border-bottom border-secondary border-md-secondary mt-4 p-0">
            <div className="p-3 col-12 col-md-4 border-bottom border-md-bottom-0 border-md-end">
                {/* Nicolette */}
                <section>
                    <h6 className="m-0">ORDER ID</h6>
                    <p>{order._id}</p>
                </section>

                <section>
                    <h6 className="m-0">ORDERED BY:</h6>
                    <p>{userName}</p>
                </section>

                <section>
                    <h6 className="m-0">ORDERED ON:</h6>
                    <p>{convertDate(order.createdOn)}</p>
                </section>
            </div>
            <div className="col-12 col-md-8 p-3">
                <section>
                    <section className="mb-1 d-flex align-items-center">
                        <CircleFill className="me-2 order__product-label-icon"/>
                        <h6 className="m-0">PRODUCTS</h6>
                    </section>
                    <section className="d-flex text-muted">
                        <h6 className="m-0 col-6 col-lg-5 me-3">NAME</h6>
                        <h6 className="m-0 col-3 col-lg-2">QUANTITY</h6>
                        <h6 className="m-0 col-3 col-lg-2">PRICE</h6>
                    </section>
                    {order.products.map(product => {
                        return (
                            <OrderItemProduct key={product.productId} product={product}/>
                        )
                    })}
                </section>

                <section className="d-flex align-items-center border-top pt-1 mt-3">
                        <div className="m-0 col-6 col-lg-5 me-3"></div>
                        <h6 className="m-0 col-3 col-lg-2">TOTAL:</h6>
                        <p className="m-0 col-3 col-lg-2">{order.totalAmount}</p>
                </section>
            </div> 
        </Container>
    )
}