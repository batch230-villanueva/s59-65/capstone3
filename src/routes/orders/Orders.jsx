import OrderItem from "./OrderItem";
import { Container } from "react-bootstrap";
import { useEffect, useState } from "react";

export default function Orders () {

    const [orders, setOrders] = useState([])

    useEffect(() => {
        
        fetch(`https://capstone-2-villanueva.onrender.com/orders/getOrders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setOrders(data);
        })
    }, [])

    return (
        <Container fluid className="order px-2 px-lg-5">
        <div className="w-100 pb-2 position-sticky order__heading">
            <p className="m-0">YOUR ORDERS</p>
        </div>

        <div>
            {orders.map(order => <OrderItem key={order._id} order={order}/>)}
        </div>

        </Container>
    )
}