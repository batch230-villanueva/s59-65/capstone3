import AppContext from "../../contexts/AppContext"
import { useContext, useState } from "react"
import { Container } from "react-bootstrap";
import { Button, ButtonGroup} from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { ImageFill } from "react-bootstrap-icons";

export default function Product() {

    const {selectedProduct, user, refreshUserDetails, refreshProducts} = useContext(AppContext);

    const navigate = useNavigate();

    const [quantity, setQuantity] = useState(0);

    const handleAddToCart = (ev) => {
        ev.preventDefault();

        if (user === null) {
            navigate('../login');
        } else {
            const token = localStorage.getItem('token')

            fetch('https://capstone-2-villanueva.onrender.com/users/addToCart', {
              method: "PUT",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
              },
              body:JSON.stringify(
                {
                    prodId: selectedProduct._id,
                    quantity: quantity,
                }
              )
            })
            .then(res => res.json())
            .then(data => {
              refreshUserDetails(token);
              refreshProducts();
              setQuantity(0)
            })
        }
    }

    const increaseQuantity = () => {
        if(quantity < selectedProduct.stocks) {
            setQuantity(quantity + 1);
        }
    }

    const decreaseQuantity = () => {
        if(quantity !== 0) {
            setQuantity(quantity - 1);
        }
    }

    return(
        <Container fluid className="d-flex flex-column flex-lg-row px-2 px-md-5 page">
            <div className="col-12 col-lg-6 bg-dark mb-4 product__img">
                <ImageFill className="text-light product__img-icon"/>
            </div>
            <div className="col-12 col-lg-6 d-flex flex-column justify-content-between product__details mx-lg-4">
                <div>
                <section className="product__section">
                    <h6>NAME</h6>
                    <p>{selectedProduct.name}</p>
                </section>

                <section className="product__section mt-4">
                    <h6>DESCRIPTION</h6>
                    <p>{selectedProduct.description}</p>
                </section>

                <section className="product__section mt-4"> 
                    <section className="d-flex">
                        <h6 className="col-4">PRICE</h6>
                        <h6 className="col-8">STOCKS</h6>
                    </section>

                    <section className="d-flex">
                        <p className="col-4">{selectedProduct.price}</p>
                        <p className="col-8">{selectedProduct.stocks}</p>
                    </section>
                    
                </section >       
                </div>      
                <>
                    {
                        (!user || !user.isAdmin) ? 
                        <div className="product__add-section d-flex justify-content-end py-3 bg-light">
                            <ButtonGroup className="me-3">
                                <Button variant="outline-dark" onClick={decreaseQuantity}>-</Button>
                                <span className="border-dark border-top border-bottom text-dark p-1 m-0 px-4 d-flex align-items-center">{quantity}</span>
                                <Button variant="outline-dark" onClick={increaseQuantity}>+</Button>
                            </ButtonGroup>
                            <Button disabled={!quantity} variant="dark" onClick={handleAddToCart}>Add To Cart</Button>
                        </div> :
                        <div className="product__add-section d-flex justify-content-end py-3 bg-light">
                            <h6>*YOU CANNOT BUY PRODUCTS AS AN ADMIN</h6>
                        </div>
                    }
                </>       
                {/* <div className="product__add-section d-flex justify-content-end py-3 bg-light">
                    <ButtonGroup className="me-3">
                        <Button variant="dark" onClick={decreaseQuantity}>-</Button>
                        <span className="bg-dark text-light p-1 m-0 px-4">{quantity}</span>
                        <Button variant="dark" onClick={increaseQuantity}>+</Button>
                    </ButtonGroup>
                    <Button variant="success" onClick={handleAddToCart}>Add To Cart</Button>
                </div> */}
            </div>
        </Container>
    )
}