import { Compass } from "react-bootstrap-icons"
import { Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom"

export default function Lost() {

    const navigate = useNavigate();

    return (
        <div className="text-center lost">
            <div className="d-flex flex-column justify-content-center align-items-center text-center lost__center">
            <Compass className="lost__icon mb-5"/>
            <h2>Not all those who wander are lost...</h2>
            <p className="m-0">Except for you, the page you are looking for does not exist.</p>
            <p>You are definitely lost.</p>
            <Button onClick={() => {navigate('/')}} className="mt-3" variant="dark" size="lg">Get back Home</Button>
            </div>
        </div>
    )
}