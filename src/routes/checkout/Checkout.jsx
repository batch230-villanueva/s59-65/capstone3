import { Container, Button } from "react-bootstrap"
import { useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import AppContext from "../../contexts/AppContext"
import CheckoutItem from "./CheckoutItem"
import CheckoutItemMobile from "./CheckoutItemMobile"
import Swal from "sweetalert2"

const getWidth = () => {
    const {innerWidth} = window
    return innerWidth
}

export default function Checkout () {

    const {user, refreshUserDetails, refreshProducts} = useContext(AppContext);

    const [width, setWidth] = useState(getWidth());

    const navigate = useNavigate();

    useEffect(() => {
        if (user.isAdmin) {
            navigate('/')
        }
    },[])

    useEffect(() => {
        const handleResize = () => {
          setWidth(getWidth())
        }
    
        window.addEventListener('resize', handleResize);
    
        return () => {
          window.removeEventListener('resize', handleResize);
        };
      }, [])

    const checkOut = () => {
        const token = localStorage.getItem('token')

        fetch(`https://capstone-2-villanueva.onrender.com/orders/order`, {
            headers: {
                Authorization: `Bearer ${token}`
              }
          })
          .then(res => res.json())
          .then(data => {
            refreshUserDetails(token);
            refreshProducts();
            console.log(data);           
      
            

            if (typeof data !== undefined) {
              Swal.fire({
                  title: "Check out Successful",
                  icon: "success",
                  text: "Thanks for supporting our merchandise!"
                })

                navigate('/');
            } else {
              Swal.fire({
                title: "Check out failed",
                icon: "error",
                text: "Retry"
              })
            }
            
            
          })
    }

    const handleProductDisplay = () => {
        if (user.cart.length === 0){
            return (
                <p>You have no items in your cart</p>
            )
        } else {
            if (width < 576) {
                return (
                    <>
                        <div className="checkout__products">
                            {user.cart.map(item => <CheckoutItemMobile key={item.productId} item={item}/>)}
                        </div>
        
                        <div className="d-flex align-items-center py-3 bg-light border-top checkout__section--mobile">
                            <div className="d-flex align-items-center w-100">
                                <section className="w-100 d-flex justify-content-between">
                                    <section>
                                    <h6 className="col-1 m-0">SUBTOTAL:</h6>
                                    <p className="m-0 col-3">{getCartTotal()}</p>
                                    </section>
                                <Button variant="dark" className="checkout__button" onClick={checkOut}>CHECKOUT</Button>
                                </section>
                            </div>
                        </div>
                    </>
                )
            } else {
                return (
                    <>
                        <div className="checkout__products">
                            <div className="d-flex pt-2">
                                <div className="col-2 me-2"></div>
                                <span className="d-flex align-items-center h-25 w-100">
                                    <h6 className="col-5">NAME</h6>
                                    <h6 className="col-1">QUANTITY</h6>
                                    <h6 className="col-3">PRICE</h6>
                                </span>
                            </div>
                            {user.cart.map(item => <CheckoutItem key={item.productId} item={item}/>)}
                        </div>
        
                        <div className="d-flex align-items-center py-3 checkout__section">
                            <div className="col-2">

                            </div>
                            <div className="d-flex align-items-center w-100">
                                <section className="col-9"></section>
                                <section className="w-100 d-flex justify-content-between">
                                    <section>
                                    <h6 className="col-1 m-0">SUBTOTAL:</h6>
                                    <p className="m-0 col-3">{getCartTotal()}</p>
                                    </section>
                                <Button variant="dark" className="checkout__button" onClick={checkOut}>CHECKOUT</Button>
                                </section>
                            </div>
                        </div>
                    </>
                )
            }
        }
    }

    const getCartTotal = () => {
        return user.cart.map(item => {return item.subTotal}).reduce((sum, cPrice) => sum + cPrice,0)
    }

    return (
        <Container fluid className="page px-md-5 checkout">
            <div className="w-100 pb-2 checkout__heading">
                <p className="m-0 ms-2">CART</p>
            </div>

            {handleProductDisplay()}
{/* 
            {
                user.cart.length !==0 && 
                <div className="d-flex align-items-center py-3 checkout__section">
                    <div className="col-2">

                    </div>
                    <div className="d-flex align-items-center w-100">
                        <section className="col-9"></section>
                        <section className="w-100 d-flex justify-content-between">
                            <section>
                            <h6 className="col-1 m-0">SUBTOTAL:</h6>
                            <p className="m-0 col-3">{getCartTotal()}</p>
                            </section>
                        <Button variant="dark" className="checkout__button" onClick={checkOut}>CHECKOUT</Button>
                        </section>
                    </div>
                </div>
            } */}
        </Container>
    )
}