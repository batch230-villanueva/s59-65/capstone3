import { Button } from "react-bootstrap"
import { useContext } from "react";
import AppContext from "../../contexts/AppContext";
import { ImageFill } from "react-bootstrap-icons";

export default function CheckoutItemMobile({item}) {

    const {refreshUserDetails, refreshProducts} = useContext(AppContext)

    const removeCartItem = () => {
        const token = localStorage.getItem('token');

        fetch('https://capstone-2-villanueva.onrender.com/users/removeCartItem', {
              method: "PATCH",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
              },
              body:JSON.stringify(
                {
                    prodId: item.productId,
                }
              )
            })
            .then(res => res.json())
            .then(data => {
              refreshUserDetails(token);
              refreshProducts();
            })
    }

    return(
        <div className="d-flex flex-column py-2 mb-2 checkout__item">
            <section className="bg-dark checkout__item-img text-light col-12 me-2">
            <ImageFill className="checkout__item-img-icon"/>

            </section>

            <section>
                <section className="d-flex justify-content-between align-items-center py-2 border-bottom">
                    <p className="fs-6  m-0">{item.productId}</p>
                    <Button variant="outline-danger" className="checkout__item-btn" onClick={removeCartItem}>REMOVE</Button>
                </section>

                <section className="d-flex mt-2">
                    <section className="col-6">
                        <h6 className="text-muted mb-1">QUANTITY</h6>
                        <p className="fs-6  m-0">{item.quantity}</p>
                    </section>
                    <section className="col-6">
                    <h6 className="text-muted mb-1">PRICE</h6>
                    <p className="fs-6 m-0">{item.subTotal}</p>
                    </section>
                </section>
            </section>
        </div>
    )
}