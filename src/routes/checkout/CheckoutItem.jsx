import { Button } from "react-bootstrap"
import { useContext, useEffect, useState } from "react";
import AppContext from "../../contexts/AppContext";
import { ImageFill } from "react-bootstrap-icons";

export default function CheckoutItem({item}) {

    const {user, refreshUserDetails, refreshProducts, getProductName} = useContext(AppContext);
    const [productName, setProductName] = useState("");

    useEffect(() => {
      getProductName(item.productId).then(result => {
        setProductName(result.productName)
      })
    }, [user.cart])

    

    const removeCartItem = () => {
        const token = localStorage.getItem('token');

        fetch('https://capstone-2-villanueva.onrender.com/users/removeCartItem', {
              method: "PATCH",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
              },
              body:JSON.stringify(
                {
                    prodId: item.productId,
                }
              )
            })
            .then(res => res.json())
            .then(data => {
              refreshUserDetails(token);
              refreshProducts();
            })
    }

    return(
        <div className="d-flex pt-2 mb-2 checkout__item">
            <section className="bg-dark checkout__item-img text-light col-2 me-2">
            <ImageFill className="checkout__item-img-icon"/>

            </section>
            <span className="d-flex align-items-center h-25 w-100">
            <p className="col-5 m-0">{productName}</p>
            <p className="col-1 m-0">{item.quantity}</p>
            <p className="col-3 m-0">{item.subTotal}</p>
            <Button variant="outline-danger" className="checkout__item-btn" onClick={removeCartItem}>REMOVE</Button>
            </span>
        </div>
    )
}