import { useContext, useEffect, useState } from "react";
import { CircleFill } from "react-bootstrap-icons";
import AppContext from "../../contexts/AppContext";
import { useNavigate } from "react-router-dom";
import HomeProductCard from "./HomeProductCard";
import { Container } from "react-bootstrap";

export default function Home () {

    // const [products, setProducts] = useState([]);
    
    // useEffect(() => {
    //     fetch('http://localhost:4000/products/getActiveProducts', {
    //       headers: {
    //         Authorization: `Bearer ${localStorage.getItem('token')}`
    //       }
    //     })
    //     .then(res => res.json())
    //     .then(data => {
    //         console.log(data);
    //         setProducts(data)})
    // },[])

    const {products, refreshProducts} = useContext(AppContext);

    useEffect(() => {
      refreshProducts();
    },[]);

    return(
        <div className="home">
          
          <div className="home__splash d-flex flex-column justify-content-center">
            <img className="home__splash-img" src="assets/images/splash.png" alt="" />
            <h1 className="home__splash-text col-12 col-lg-6 px-1 px-md-5">Mankind be Vigilant.</h1>
          </div>
          <div className="home__text-cont d-flex m-2 mb-5 m-md-5 py-2">
            <section className="col-0 col-md-3 col-lg-6">

            </section>
            <p className="col-12 col-md-9 col-lg-6 text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          </div>

          <div className="px-2 px-lg-5 mb-4" id="products">
            <div className="d-flex align-items-center container-fluid pb-2 mb-4 home__products-heading">
              <CircleFill/>
              <h2 className="m-0 ms-4">PRODUCTS</h2>
            </div>
          
          <Container fluid className="d-md-flex h-100 home__products-cont">
            {products.map(product => <HomeProductCard key={product._id} cardProduct={product}/>)}
          </Container>
          </div>

          <footer className="home__footer bg-dark text-light">
            <p>Coded by emvil-git</p>
            <p>Disco Elysium and all related names are property of Studio ZA/UM and are only used here for Non-Commercial Educational Purposes only.</p>
          </footer>
        </div>
    )
}