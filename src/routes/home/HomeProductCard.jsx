import { Button } from "react-bootstrap"
import { useContext, useEffect } from "react"
import AppContext from "../../contexts/AppContext"
import { useNavigate } from "react-router-dom"
import { CircleFill, ImageFill } from "react-bootstrap-icons"

export default function HomeProductCard ({cardProduct}) {

    const {setSelectedProduct} = useContext(AppContext);

    const navigate = useNavigate();

    const handleShowProductDetails = (ev) => {
        ev.preventDefault();

        fetch(`https://capstone-2-villanueva.onrender.com/products/${cardProduct._id}`)
        .then(res => res.json())
        .then(data => {
            setSelectedProduct(data)
            navigate('/product')
        })
    }  

    return (
        <section className="col-12 col-sm-6 col-md-4 col-lg-3 d-flex flex-column justify-items-between mb-4 me-2 me-lg-0 px-lg-2">
          <section className="bg-dark home__product-card-img text-light">
            <ImageFill className="home__product-card-img-icon"/>
          {/* <p>{cardProduct.description}</p>
          
          <p>{cardProduct.stocks}</p> */}
          </section>

          <section className="d-flex justify-content-between align-items-center w-100 mt-2 home__product-card-sect" onClick={handleShowProductDetails}>
            <section className="pe-1">
                <p className="m-0 text-truncate text-uppercase home__product-card-name">{cardProduct.name}</p>
                <p className="m-0 home__product-card-price">{`PHP ${cardProduct.price}`}</p>
            </section>
            {/* <Button class="rounded-0" variant="dark" onClick={handleShowProductDetails}>See More</Button> */}
            <CircleFill/>
          </section>
        </section>
      )
}