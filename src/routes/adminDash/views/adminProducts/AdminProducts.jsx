import { useContext, useState } from "react"
import {Button} from "react-bootstrap"
import AdminProductsModal from "./AdminProductsModal";
import AdminProductCard from "./AdminProductCard";
import AdminContext from "../../../../contexts/AdminContext";
import { CircleFill } from "react-bootstrap-icons";

export default function AdminProducts () {

    const {adminProducts} = useContext(AdminContext);

    const [modalShow, setModalShow] = useState(false);

    const showProducts = () => {
        if(adminProducts.length !== 0){
            return adminProducts.map(product => <AdminProductCard key={product._id} product={product}/>)
        }
    }
    
    return(
        <div className="admin-products d-flex flex-column">
            <AdminProductsModal show={modalShow} onHide={() => setModalShow(false)}/>
            <section className="d-flex justify-content-between px-5 py-2 border-bottom border-dark">
                <section className="d-flex align-items-center">
                    <CircleFill/>
                    <h3 className="m-0 ms-2">AdminProducts</h3>
                </section>
                <Button className="me-5" variant="dark" onClick={() => setModalShow(true)}>Add Product</Button>
            </section>

            <section className="admin-products__cont">
                {showProducts()}
            </section>
        </div>
    )
}