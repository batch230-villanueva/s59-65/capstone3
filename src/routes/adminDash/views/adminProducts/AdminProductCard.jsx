import { useState, useContext } from "react"
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";
import AdminContext from "../../../../contexts/AdminContext";
import { Circle, CircleFill } from "react-bootstrap-icons";

export default function AdminProductCard ({product}) {
    const [isEditing, setIsEditing] = useState(false);

    // const pNameRef = useRef();
    // const pDescRef = useRef();
    // const pPriceRef = useRef();
    // const pStocksRef = useRef();

    const {adminRetrieveProducts} = useContext(AdminContext)

    const [pName, setPName] = useState(product.name);
    const [pDesc, setPDesc] = useState(product.description);
    const [pPrice, setPPrice] = useState(product.price);
    const [pStocks, setPStocks] = useState(product.stocks);

    const handleProductUpdate = (ev) => {
      ev.preventDefault();
  
      fetch(`https://capstone-2-villanueva.onrender.com/products/updateInfo/${product._id}`, {
          method: "PATCH",
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            "Content-type": "application/json"
          },
          body: JSON.stringify({
            name: pName,
            description: pDesc,
            price: pPrice,
            stocks: pStocks
          })
        })
        .then(res => res.json())
        .then(data => {
          setIsEditing(false);
          adminRetrieveProducts();
          console.log(data);          
      })
    }

    const handleArchive = (ev) => {
        ev.preventDefault();
    
        fetch(`https://capstone-2-villanueva.onrender.com/products/archive/${product._id}`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
          })
          .then(res => {
            console.log(res.status)
            
            if(res.status === 200) {
                adminRetrieveProducts();
                
                Swal.fire({
                    title: "Product Archived",
                    icon: "success",
                    text: ""
                })
            } else {
                Swal.fire({
                    title: "Failed to Archive Product",
                    icon: "error",
                    text: ""
                })
            }
          })
    }

    const handleUnarchive = (ev) => {
        ev.preventDefault();
    
        fetch(`https://capstone-2-villanueva.onrender.com/products/unarchive/${product._id}`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
          })
          .then(res => {
            console.log(res.status)
            
            if(res.status === 200) {
                adminRetrieveProducts();

                Swal.fire({
                    title: "Product Removed from Archive",
                    icon: "success",
                    text: ""
                })
            } else {
                Swal.fire({
                    title: "Failed to Remove Product from Archive",
                    icon: "error",
                    text: ""
                })
            }
          })
    }


    const handleEditing = () => {
        if(isEditing) {
            return(
                <Form className="px-5 my-4 pb-2 admin-products__card" onSubmit={handleProductUpdate}>
                  <Form.Group className="mb-3 col-10" controlId="formProdName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control type="pName" placeholder="Enter product name" value={pName} onChange={(ev) => {setPName(ev.target.value)}}/>
                  </Form.Group>

                  <Form.Group className="mb-3 col-10" controlId="formProdDesc">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Control as="textarea" rows={3} placeholder="Enter Description" value={pDesc} onChange={(ev) => {setPDesc(ev.target.value)}}/>
                  </Form.Group>

                  <div className="d-flex justify-content-between col-10">
                    <Form.Group className="mb-3 col-6 pe-1" controlId="formProdPrice">
                      <Form.Label>Price</Form.Label>
                      <Form.Control type="number" placeholder="Enter Price" 
                      value={pPrice} onChange={(ev) => {setPPrice(ev.target.value)}}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3 col-6 ps-1" controlId="formProdStocks">
                      <Form.Label>Stocks</Form.Label>
                      <Form.Control type="number" placeholder="Stocks" value={pStocks} onChange={(ev) => {setPStocks(ev.target.value)}}/>
                    </Form.Group>
                  </div>



                  <Button variant="dark" type="submit">
                    Submit
                  </Button>
                </Form>
            )
        } else {
            return (
                <Form className="px-5 my-4 pb-2 admin-products__card">
                  <Form.Group className="d-flex flex-column mb-3" controlId="formProdName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Text>{product.name}</Form.Text>
                  </Form.Group>
  
                  <Form.Group className="d-flex col-8 flex-column mb-3" controlId="formProdDesc">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Text>{product.description}</Form.Text>
                  </Form.Group>
  
                  <div className="d-flex">
                  <Form.Group className="d-flex flex-column mb-3 col-2" controlId="formProdPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Text>{product.price}</Form.Text>
                  </Form.Group>
  
                  <Form.Group className="d-flex flex-column mb-3 col-2"  controlId="formProdStocks">
                    <Form.Label>Stocks</Form.Label>
                    <Form.Text>{product.stocks}</Form.Text>
                  </Form.Group>
                  </div>

                  <Form.Group className="d-flex flex-column mb-3" controlId="formProdIsActive">
                    <Form.Label>is Active</Form.Label>
                    <Form.Text>{product.isActive ? <CircleFill/> : <Circle/>}</Form.Text>
                  </Form.Group>

                  <Form.Group className="d-flex flex-column mb-3" controlId="formProdDate">
                    <Form.Label>Created on</Form.Label>
                    <Form.Text>{product.createdOn}</Form.Text>
                  </Form.Group>
  
                  <Form.Group className="d-flex justify-content-end col-8">
                    <Button className="me-2" variant="dark" onClick={() => setIsEditing(true)}>
                      Edit
                    </Button>
                    {
                        (product.isActive) ?
                        <Button variant="outline-dark" onClick={handleArchive}>
                        Archive
                        </Button>
                        : 
                        <Button variant="outline-dark" onClick={handleUnarchive}>
                        Unarchive
                        </Button>
                    }
                  </Form.Group>
              </Form>
            )
        }
    }

    return(
        <section key={product._id}>
            {/* <p>{product.name}</p>
            <p>{product.description}</p>
            <p>{product.price}</p>
            <p>{product.stocks}</p>
            <p>{product.isActive}</p>
            <p>{product.createdOn}</p> */}

            {handleEditing()}
        </section>
    )
}