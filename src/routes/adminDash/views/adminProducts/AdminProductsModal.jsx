import {Button, Modal, Form} from 'react-bootstrap';
import { useRef, useState } from 'react';
import Swal from 'sweetalert2';
import { useContext } from 'react';
import AdminContext from '../../../../contexts/AdminContext';

export default function AdminProductsModal(props) {
  const pNameRef = useRef();
  const pDescRef = useRef();
  const pPriceRef = useRef();
  const pStocksRef = useRef();

  const {adminRetrieveProducts} = useContext(AdminContext);

  const handleSubmit = (ev) => {
    ev.preventDefault();

    console.log(pNameRef.current.value);
    console.log(pDescRef.current.value);
    console.log(typeof pPriceRef.current.value);
    console.log(typeof pStocksRef.current.value);

    fetch(`https://capstone-2-villanueva.onrender.com/products/addProduct`, {
        method: "POST",
        headers: {
            "Content-type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            name: pNameRef.current.value,
            description: pDescRef.current.value,
            price: parseInt(pPriceRef.current.value),
            stocks: parseInt(pStocksRef.current.value)
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        
        if (data) {

          adminRetrieveProducts();

          Swal.fire({
              title: "Product Added",
              icon: "success",
              text: ""
            })
        } else {
          Swal.fire({
            title: "Failed to Add Product",
            icon: "error",
            text: ""
          })
        }
      })
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className='rounded-0'
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Product
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formProdName">
            <Form.Label>Product Name</Form.Label>
            <Form.Control ref={pNameRef} type="pName" placeholder="Enter product name" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formProdDesc">
            <Form.Label>Product Description</Form.Label>
            <Form.Control ref={pDescRef} type="text" placeholder="Enter Description" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formProdPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control ref={pPriceRef} type="number" placeholder="Enter Price" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formProdStocks">
            <Form.Label>Stocks</Form.Label>
            <Form.Control ref={pStocksRef} type="number" placeholder="Stocks" />
          </Form.Group>

          <Button variant="dark" type="submit">
            Submit
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}