import { useState, useEffect, useContext } from "react"
import AppContext from "../../../../contexts/AppContext";
export default function AdminOrderProductItem ({product}) {

    const {getProductName, getProductPrice} = useContext(AppContext);

    const [productName, setProductName] = useState("");

    const [productTotal, setProductTotal] = useState();

    useEffect(() => {
      getProductName(product.productId).then(result => {
        setProductName(result.productName)
      })
    }, [])

    useEffect(() => {
      getProductPrice(product.productId).then(result => {
        setProductTotal(result.productPrice * product.quantity)
      })
    }, [])

    return (
        <section className="d-flex my-1 border-top">
            <p className="m-0 col-6 col-lg-5 me-3">{productName}</p>
            <p className="m-0 col-3 col-lg-2">{product.quantity}</p>
            <p className="m-0 col-3 col-lg-2">{productTotal}</p>
        </section>
    )
}