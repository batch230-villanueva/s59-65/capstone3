import { Button } from "react-bootstrap"
import { CircleFill } from "react-bootstrap-icons"
import { useContext, useState } from "react"
import AdminContext from "../../../../contexts/AdminContext"
import AdminOrderCard from "./AdminOrderCard"

export default function AdminOrders () {

    const {adminOrders} = useContext(AdminContext);

    const showOrders = () => {
        if(adminOrders.length !== 0){
            return adminOrders.map(order => <AdminOrderCard key={order._id} order={order}/>)
        }
    }

    

    return(
        <div className="admin-orders d-flex flex-column">
            <section className="d-flex justify-content-between px-5 py-2 border-bottom border-dark">
                <section className="d-flex align-items-center">
                    <CircleFill/>
                    <h3 className="m-0 ms-2">Orders</h3>
                </section>
            </section>

            <section className="admin-orders__cont">
                {showOrders()}
            </section>
        </div>
    )
}