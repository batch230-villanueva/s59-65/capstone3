import { Button } from "react-bootstrap"
import { CircleFill } from "react-bootstrap-icons"
import { useContext, useState } from "react"
import AdminContext from "../../../../contexts/AdminContext"
import AdminUserCard from "./AdminUsersCard"

export default function AdminUsers () {

    const {adminUsers} = useContext(AdminContext);

    const showUsers = () => {
        if(adminUsers.length !== 0){
            return adminUsers.map(user => <AdminUserCard key={user._id} user={user}/>)
        }
    }

    

    return(
        <div className="admin-users d-flex flex-column">
            <section className="d-flex justify-content-between px-5 py-2 border-bottom border-dark">
                <section className="d-flex align-items-center">
                    <CircleFill/>
                    <h3 className="m-0 ms-2">AdminUsers</h3>
                </section>
            </section>

            <section className="admin-users__cont">
                {showUsers()}
            </section>
        </div>
    )
}