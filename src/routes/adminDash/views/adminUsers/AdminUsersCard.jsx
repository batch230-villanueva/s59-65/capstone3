import { useState, useContext } from "react"
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";
import AdminContext from "../../../../contexts/AdminContext";
import { Circle, CircleFill } from "react-bootstrap-icons";

export default function AdminUserCard ({user}) {

    const {adminRetrieveUsers} = useContext(AdminContext);

    const handleSetAdmin = (ev) => {
        ev.preventDefault();
    
        fetch(`https://capstone-2-villanueva.onrender.com/users/setAdmin/${user._id}`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
          })
          .then(res => {
            
            if(res.status === 200) {
                adminRetrieveUsers();
                
                Swal.fire({
                    title: "Admin Rights Granted",
                    icon: "success",
                    text: ""
                })
            } else {
                Swal.fire({
                    title: "Error granting Admin Rights",
                    icon: "error",
                    text: ""
                })
            }
          })
    }

    const handleRemoveAdmin = (ev) => {
        ev.preventDefault();
    
        fetch(`https://capstone-2-villanueva.onrender.com/users/removeAdmin/${user._id}`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
          })
          .then(res => {
            
            if(res.status === 200) {
                adminRetrieveUsers();

                Swal.fire({
                    title: "Admin Rights Removed",
                    icon: "success",
                    text: ""
                })
            } else {
                Swal.fire({
                    title: "Failed to remove Admin Rights",
                    icon: "error",
                    text: ""
                })
            }
          })
    }

    return(
        <section key={user._id}>
            <Form className="px-5 my-4 pb-2 admin-products__card">
                  <Form.Group className="d-flex flex-column mb-3" controlId="formProdName">
                    <Form.Label className="m-0">Name</Form.Label>
                    <Form.Text>{`${user.firstName} ${user.lastName}`}</Form.Text>
                  </Form.Group>
  
                    <div className="d-flex">
                    <Form.Group className="d-flex col-3 flex-column mb-3" controlId="formProdDesc">
                        <Form.Label className="m-0">Email</Form.Label>
                        <Form.Text>{user.email}</Form.Text>  
                    </Form.Group>
                    <Form.Group className="d-flex flex-column mb-3 col-2" controlId="formProdPrice">
                    <Form.Label className="m-0">Mobile</Form.Label>
                        <Form.Text>{user.mobileNo}</Form.Text>
                    </Form.Group>
                    </div>
  
                  
                  <Form.Group className="d-flex align-items-center mb-3" controlId="formProdIsActive">
                    <Form.Label className="m-0">Admin Status</Form.Label>
                    <Form.Text className="m-0 ms-2">{user.isAdmin ? <CircleFill/> : <Circle/>}</Form.Text>
                    <Form.Group className="d-flex justify-content-end col-8">
                    {!user.isAdmin ? 
                        <Button className="me-2" size="sm" variant="dark" onClick={handleSetAdmin}>
                        Set as Admin
                        </Button> :
                        <Button className="me-2" size="sm" variant="danger" onClick={handleRemoveAdmin}>
                        Remove as Admin
                        </Button>
                    }
                  </Form.Group>
                  </Form.Group>
              </Form>
        </section>
    )
}