import { Container, Nav } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { AdminProvider } from "../../contexts/AdminContext";
import AppContext from "../../contexts/AppContext";
import AdminProducts from "./views/adminProducts/AdminProducts";
import AdminUsers from "./views/adminUsers/AdminUsers";
import AdminOrders from "./views/adminOrders/AdminOrders";
import { Navigate } from "react-router-dom";

export default function AdminDash () {

    const [view, setView] = useState('dash');

    const [adminProducts, setAdminProducts] = useState([]);
    const [adminUsers, setAdminUsers] = useState([]);
    const [adminOrders, setAdminOrders] = useState([]);

    const {user} = useContext(AppContext);

    useEffect(() => {
        fetch('https://capstone-2-villanueva.onrender.com/products/getAllProducts', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
            setAdminProducts(data)})
    },[])

    useEffect(() => {
        fetch('https://capstone-2-villanueva.onrender.com/users/getUsers', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
            setAdminUsers(data)})
    },[])

    useEffect(() => {
        fetch('https://capstone-2-villanueva.onrender.com/orders/getAllOrders', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
            setAdminOrders(data)})
    },[])

    const adminRetrieveUsers = () => {
        fetch('https://capstone-2-villanueva.onrender.com/users/getUsers', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
            setAdminUsers(data)})
    }
    const adminRetrieveProducts = () => {
        fetch('https://capstone-2-villanueva.onrender.com/products/getAllProducts', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
            setAdminProducts(data)})
    }
    const adminRetrieveOrders = () => {
        fetch('https://capstone-2-villanueva.onrender.com/orders/getAllOrders', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
            setAdminOrders(data)})
    }

    const showView = () => {
        switch(view){
            case 'dash':
                return(
                    <>
                        <h1>Dashboard</h1>
                    </>
                )
            case 'products':
                return(
                    <AdminProducts/>
                )
            case 'users':
                return(
                    <AdminUsers/>
                )
            case 'orders':
                return(
                    <AdminOrders/>
                )
        }
    }

    return(
        <>
            {(!user.isAdmin) ? <Navigate to="/"/> :
                <AdminProvider value={{adminProducts, setAdminProducts, adminUsers, setAdminUsers, adminOrders, setAdminOrders, adminRetrieveUsers, adminRetrieveProducts, adminRetrieveOrders}}>
                <Container fluid className="d-flex p-0 admin">
                    <Nav className="d-flex flex-column col-2  bg-dark  text-light admin__nav">
                        <Nav.Item>ADMIN</Nav.Item>
                        <Nav.Item>
                            <Nav.Link onClick={()=>{setView('dash')}}>
                                Dashboard
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link onClick={()=>{setView('products')}}>
                                Products
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link onClick={()=>{setView('users')}}>
                                Users
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link onClick={()=>{setView('orders')}}>
                                Orders
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                    <div className="col-10 admin__view">
                        {showView()}
                    </div>
                </Container>
            </AdminProvider>
            }
        </>
    )
}