import { useEffect, useState } from 'react';
import { Button, Form, Container } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import AppContext from '../../contexts/AppContext';
// import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const baseUrl = "http://localhost:4000";

export default function Login() {
    //2
    const { user, setUser } = useContext(AppContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isEnabled, setIsEnabled] = useState(false);

    const navigate = useNavigate();

  useEffect(() => {
    if (email !== '' && password !== '') {
        setIsEnabled(true)
    } else {
        setIsEnabled(false)
    }
  }, [email, password])

  const handleLogin = async (ev) => {
    ev.preventDefault();

    fetch(`https://capstone-2-villanueva.onrender.com/users/login`, {
      method: "POST",
      headers: {"Content-type": "application/json"},
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      console.log(data.access);
      

      if (data) {
        localStorage.setItem('token', data.access);
        retrieveUserDetails(data.access);
        Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Let's Disco!"
          })
      } else {
        Swal.fire({
          title: "Authentication failed",
          icon: "error",
          text: "Check your login details"
        })
      }
      
      
    })

    setEmail('');
    setPassword('');
    console.log(user);
  }

  const retrieveUserDetails = (token) => {
    fetch('https://capstone-2-villanueva.onrender.com/users/getUser', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      (data.isAdmin) ? navigate('/admin') : navigate('/')

      const logUser = {
        name: `${data.firstName} ${data.lastName}`,
        id: data._id,
        isAdmin: data.isAdmin,
        cart: data.cart,
      }

      localStorage.setItem('user',JSON.stringify(logUser))

      setUser(logUser);

      // (data.isAdmin) ? navigate('/admin') : navigate('/')
    })
  }


  return (
    <div className='login'>
      <Container fluid className='p-3 p-md-5 border rounded'>
        <h4 className='w-100 text-center mb-4'>Welcome Back!</h4>
          <Form onSubmit={handleLogin}>
          <Form.Group className="mb-2" controlId="formBasicEmail">
            <Form.Label><h6 className='m-0'>Email</h6></Form.Label>
            <Form.Control type="email" placeholder="Enter email" value={email} onChange={(ev) => {setEmail(ev.target.value)}}/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label><h6 className='m-0'>Password</h6></Form.Label>
            <Form.Control type="password" placeholder="Password" value={password} onChange={(ev) => {setPassword(ev.target.value)}}/>
          </Form.Group>

          <Form.Text>
            Don't have an account? <a href="#" onClick={() => navigate('/register')}>Register</a> to get started.
          </Form.Text>

          <Button className='mt-4 w-100' size='lg' variant="dark" type="submit" disabled={!isEnabled}>
                Log In
          </Button>

        </Form>
      </Container>
    </div>
  );
}