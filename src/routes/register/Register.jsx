import { useState, useContext } from 'react';
import { Button, Form, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');

    // const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate();

    const handleLoginEnable = () => {
        if (email !== '' && password !== '' && password2 !== '') return (
            <Button className='mt-4 w-100' size='lg' variant="dark" type="submit">
                Sign Up
            </Button>
        )

        return (
            <Button className='mt-4 w-100' size='lg' variant="dark" type="submit" disabled>
                Sign Up
            </Button>
        )
    }

  const checkMobileNo = (mobile) => {
    const mobileRegexIntl = /^([+]\d{2})?\d{10}$/
    const mobileRegex= /\d{11,}$/

    if (mobileRegex.test(mobile) || mobileRegexIntl.test(mobile)) return true;

    return false;
  }

  const checkEmail = () => {
    fetch(`https://capstone-2-villanueva.onrender.com/users/checkEmail`, {
      method: "POST",
      headers: {"Content-type": "application/json"},
      body: JSON.stringify({
        email: email
      })
    }).then(res => res.json())
    .then(data => {
      console.log(data)
      return(data)
    })
  }

  const checkAndRegister = (ev) => {
    ev.preventDefault()

    fetch(`https://capstone-2-villanueva.onrender.com/users/checkEmail`, {
      method: "POST",
      headers: {"Content-type": "application/json"},
      body: JSON.stringify({
        email: email
      })
    }).then(res => res.json())
    .then(data => {
      console.log(data)
      if (data === false) {
        if (password === password2 && checkMobileNo(mobile)) {
          fetch(`https://capstone-2-villanueva.onrender.com/users/signup`, {
            method: "POST",
            headers: {"Content-type": "application/json"},
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobile,
              password: password
            })
          })
          .then(res => res.json())
          .then(data => {
            console.log(data);    
      
            if (data) {
              Swal.fire({
                  title: "Sign Up Successful",
                  icon: "success",
                  text: "Log In to Access your Account"
                })
            } else {
              Swal.fire({
                title: "Registration failed",
                icon: "error",
                text: "Check your sign up details"
              })
            }
          })
        }
    
        setFirstName('');
        setLastName('');
        setEmail('');
        setMobile('');
        setPassword('');
        setPassword2('');
    
        navigate('/login');
      } else {
        setFirstName('');
        setLastName('');
        setEmail('');
        setMobile('');
        setPassword('');
        setPassword2('');
  
        Swal.fire({
          title: "Registration failed",
          icon: "error",
          text: "Email is already registered"
        })
      }
    })
  }

  const handleRegister = (ev) => {
    ev.preventDefault();

    if (!checkEmail()) {
      if (password === password2 && checkMobileNo(mobile)) {
        fetch(`https://capstone-2-villanueva.onrender.com/users/signup`, {
          method: "POST",
          headers: {"Content-type": "application/json"},
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNo: mobile,
            password: password
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data);    
    
          if (data) {
            Swal.fire({
                title: "Sign Up Successful",
                icon: "success",
                text: "Log In to Access your Account"
              })
          } else {
            Swal.fire({
              title: "Registration failed",
              icon: "error",
              text: "Check your sign up details"
            })
          }
        })
      }
  
      setFirstName('');
      setLastName('');
      setEmail('');
      setMobile('');
      setPassword('');
      setPassword2('');
  
      navigate('/login');
    } else {
      setFirstName('');
      setLastName('');
      setEmail('');
      setMobile('');
      setPassword('');
      setPassword2('');

      Swal.fire({
        title: "Registration failed",
        icon: "error",
        text: "Email is already registered"
      })
    }
  }

  return (
    <div className='register'>
        <Container fluid className='w-100 p-3 p-md-5 border rounded'>
        <h4 className='w-100 text-center mb-4'>Create an Account</h4>
        {/* {(cUser.id !== null) ?
          <Navigate to="/courses"/> : */}
          <Form onSubmit={checkAndRegister}>

            <Form.Label><h6 className='m-0'>Name</h6></Form.Label>
            <div className='d-flex mb-2'>
              <Form.Group className="col-6 pe-1" controlId="formBasicFirstName">
                <Form.Control required type="text" placeholder="First name" value={firstName} onChange={(ev) => {setFirstName(ev.target.value)}}/>
              </Form.Group>
              
              <Form.Group className="col-6 ps-1" controlId="formBasicLastName">
                <Form.Control required type="text" placeholder="Last name" value={lastName} onChange={(ev) => {setLastName(ev.target.value)}}/>
              </Form.Group>
            </div>
              
              
              <Form.Group className="mb-2" controlId="formBasicEmail">
                <Form.Label><h6 className='m-0'>Email</h6></Form.Label>
                <Form.Control required type="email" placeholder="address@mail.com" value={email} onChange={(ev) => {setEmail(ev.target.value)}}/>
              </Form.Group>
              
              <Form.Group className="mb-2" controlId="formBasicMobile">
                <Form.Label><h6 className='m-0'>Mobile No.</h6></Form.Label>
                <Form.Control required type="text" placeholder="0999 999 9999" value={mobile} onChange={(ev) => {setMobile(ev.target.value)}}/>
              </Form.Group>
          
              <Form.Group className="mb-2" controlId="formBasicPassword">
                <Form.Label><h6 className='m-0'>Password</h6></Form.Label>
                <Form.Control required type="password" placeholder="Password" value={password} onChange={(ev) => {setPassword(ev.target.value)}}/>
              </Form.Group>
          
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label><h6 className='m-0'>Re-enter Password</h6></Form.Label>
                <Form.Control required type="password" placeholder="Password" value={password2} onChange={(ev) => {setPassword2(ev.target.value)}}/>
              </Form.Group>
          
              <Form.Text>
                Already have an account? <a href="#" onClick={() => navigate('/login')}>Log in</a> instead.
              </Form.Text>

              {handleLoginEnable()}
              {/* <Button variant="primary" type="submit">
                Submit
              </Button> */}
            </Form>
        {/* } */}
        </Container>
    </div>
  );
}